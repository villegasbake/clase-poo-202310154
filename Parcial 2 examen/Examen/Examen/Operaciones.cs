﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Examen
{
    class Operaciones
    {
        private int tenis;

        public Operaciones(int tenis)
        {
            this.tenis = tenis;
        }

        public int CalcularPrecio()
        {
            int resultado = 0;
            switch (this.tenis)
            {
                case 0:
                    return 1000;
                    break;

                case 1:
                    return 800;
                    break;

                case 2:
                    return 950;
                    break;

                case 3:
                    return 1200;
                    break;

                default:
                    resultado = 0;
                    break;
            }
            return resultado;
        }


    }
}
