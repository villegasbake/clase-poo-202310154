﻿
namespace Examen
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_Comprar = new System.Windows.Forms.Button();
            this.lblTotal = new System.Windows.Forms.Label();
            this.combo = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // btn_Comprar
            // 
            this.btn_Comprar.BackColor = System.Drawing.Color.Turquoise;
            this.btn_Comprar.Font = new System.Drawing.Font("Segoe Print", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Comprar.Location = new System.Drawing.Point(75, 171);
            this.btn_Comprar.Name = "btn_Comprar";
            this.btn_Comprar.Size = new System.Drawing.Size(121, 62);
            this.btn_Comprar.TabIndex = 0;
            this.btn_Comprar.Text = "Comprar tenis";
            this.btn_Comprar.UseVisualStyleBackColor = false;
            this.btn_Comprar.Click += new System.EventHandler(this.button1_Click);
            // 
            // lblTotal
            // 
            this.lblTotal.AutoSize = true;
            this.lblTotal.Font = new System.Drawing.Font("Segoe Print", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotal.Location = new System.Drawing.Point(101, 257);
            this.lblTotal.Name = "lblTotal";
            this.lblTotal.Size = new System.Drawing.Size(80, 26);
            this.lblTotal.TabIndex = 2;
            this.lblTotal.Text = "Total . . .";
            // 
            // combo
            // 
            this.combo.BackColor = System.Drawing.Color.Turquoise;
            this.combo.Font = new System.Drawing.Font("Segoe Print", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.combo.FormattingEnabled = true;
            this.combo.Items.AddRange(new object[] {
            "Nike ",
            "Converse",
            "Adidas",
            "Pirma"});
            this.combo.Location = new System.Drawing.Point(75, 77);
            this.combo.Name = "combo";
            this.combo.Size = new System.Drawing.Size(121, 34);
            this.combo.TabIndex = 3;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightGreen;
            this.ClientSize = new System.Drawing.Size(287, 390);
            this.Controls.Add(this.combo);
            this.Controls.Add(this.lblTotal);
            this.Controls.Add(this.btn_Comprar);
            this.Name = "Form1";
            this.Text = "Tienda de Tenis";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_Comprar;
        private System.Windows.Forms.Label lblTotal;
        private System.Windows.Forms.ComboBox combo;
    }
}

