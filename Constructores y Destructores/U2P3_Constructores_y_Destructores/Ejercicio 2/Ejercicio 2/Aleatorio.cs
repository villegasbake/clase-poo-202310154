﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio_2
{
    class Aleatorio
    {
        //Abstraer la clase Random
        private Random ran = new Random();
        private int lim=9;
        //Generar 10 números aleatorio entre 0 y 250
        //Los almaceno en un arrelgo para despues imprimir

       

        private int[] arregloNum = new int[10];

        public void ImprimeAleatorio()
        {
            Console.WriteLine("Números Aleatorios");
            for (int i = 0; i < lim; i++)
            {
                arregloNum[i] = ran.Next(250);
            }

            foreach (var value in arregloNum)
            {
                Console.WriteLine(value);
            }
        }



        
        public Aleatorio (int lim)
        {
            //Acople para agregar el destructor
            this.lim = lim;
        }
        ~Aleatorio()
        {
            //Destructor
            lim = 0;
        }
    }
}
