﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio_1
{
    class Prueba
    {
        public int x;
        public Prueba(int x)
        {
            this.x = x;
            System.Console.WriteLine("Creado objeto Prueba con x=(0)", x);
            Console.ReadLine();
        }

        ~Prueba()
        {
            this.x = 0;
        }

    }
    
}
