﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Examen_Practico
{
    public partial class Form1 : Form
    {
       
        public Form1()
        {
            InitializeComponent();
        }

        private void btn_arranque_Click(object sender, EventArgs e)
        {
            Refrigerador Refri = new Refrigerador();
            Refri.setColor("Blanco");
            string colorRefri = Refri.getColor();

            Refri.setTamano(200);
            int tamanoRefri = Refri.getTamano();

            Refri.setPuertas(2);
            int puertasRefri = Refri.getPuertas();

            Refri.setBebidas(12);
            int bebidasRefri = Refri.getBebidas();

            Refri.setAlimentos(30);
            int alimentosRefri = Refri.getAlimentos();

            MessageBox.Show("El Refrigerador es de color " + colorRefri);
            MessageBox.Show("El tamaño es de " + tamanoRefri + " cm de alto ");
            MessageBox.Show("Tiene " + puertasRefri + " puertas");
            MessageBox.Show("Tiene en su interior " + bebidasRefri + " bebidas");
            MessageBox.Show("Tiene en su interior " + alimentosRefri + " alimentos");
            MessageBox.Show("Gracias por abrir el refrigerador");
        }
    }
}
