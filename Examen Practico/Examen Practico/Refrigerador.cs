﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Examen_Practico
{
    class Refrigerador
    {
        public string color = "";
        public int tamano = 0;
        public int puertas = 0;
        public int bebidas = 0;
        public int alimentos = 0;


        public void setColor(string color)
        {
            this.color = color;
        }

        public string getColor()
        {
            return this.color;
        }

        public void setTamano(int tamano)
        {
            this.tamano = tamano;
        }

        public int getTamano()
        {
            return this.tamano;
        }

        public void setPuertas(int puertas)
        {
            this.puertas = puertas;
        }

        public int getPuertas()
        {
            return this.puertas;
        }

        public void setBebidas(int bebidas)
        {
            this.bebidas = bebidas;
        }

        public int getBebidas()
        {
            return this.bebidas;
        }

        public void setAlimentos(int alimentos)
        {
            this.alimentos = alimentos;
        }

        public int getAlimentos()
        {
            return this.alimentos;
        }
    }
}
