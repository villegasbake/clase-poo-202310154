﻿
namespace Constructores
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.cbxMultas = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblResultado = new System.Windows.Forms.Label();
            this.btnCalcularMulta = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // cbxMultas
            // 
            this.cbxMultas.BackColor = System.Drawing.SystemColors.ControlDark;
            this.cbxMultas.FormattingEnabled = true;
            this.cbxMultas.Items.AddRange(new object[] {
            "Mal estacionamiento",
            "Exceso de velocidad",
            "Pasarse un alto",
            "Exceso de alcohol"});
            this.cbxMultas.Location = new System.Drawing.Point(298, 72);
            this.cbxMultas.Name = "cbxMultas";
            this.cbxMultas.Size = new System.Drawing.Size(121, 21);
            this.cbxMultas.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe Print", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(131, 77);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(147, 26);
            this.label1.TabIndex = 1;
            this.label1.Text = "Seleccionar multa";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe Print", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(160, 129);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(99, 26);
            this.label2.TabIndex = 2;
            this.label2.Text = "Pago multa";
            // 
            // lblResultado
            // 
            this.lblResultado.AutoSize = true;
            this.lblResultado.Font = new System.Drawing.Font("Segoe Print", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblResultado.Location = new System.Drawing.Point(331, 129);
            this.lblResultado.Name = "lblResultado";
            this.lblResultado.Size = new System.Drawing.Size(36, 26);
            this.lblResultado.TabIndex = 3;
            this.lblResultado.Text = ". . .";
            // 
            // btnCalcularMulta
            // 
            this.btnCalcularMulta.BackColor = System.Drawing.SystemColors.ControlDark;
            this.btnCalcularMulta.Font = new System.Drawing.Font("Segoe Print", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCalcularMulta.Location = new System.Drawing.Point(298, 189);
            this.btnCalcularMulta.Name = "btnCalcularMulta";
            this.btnCalcularMulta.Size = new System.Drawing.Size(121, 88);
            this.btnCalcularMulta.TabIndex = 4;
            this.btnCalcularMulta.Text = "Calcular Multa";
            this.btnCalcularMulta.UseVisualStyleBackColor = false;
            this.btnCalcularMulta.Click += new System.EventHandler(this.btnCalcularMulta_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.MediumTurquoise;
            this.ClientSize = new System.Drawing.Size(599, 347);
            this.Controls.Add(this.btnCalcularMulta);
            this.Controls.Add(this.lblResultado);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cbxMultas);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cbxMultas;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblResultado;
        private System.Windows.Forms.Button btnCalcularMulta;
    }
}

