﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsExceptions05
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnCerrar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        
        private void btnLIMPIAR_Click(object sender, EventArgs e)
        {
            textBDESCRIPCION.Clear();
            textBHORA.Clear();
            textBMINUTO.Clear();
            textDIA.Clear();
        }

        private void btnAgendar_Click(object sender, EventArgs e)
        {
            try
            {
                int Hora; 
                int Minuto;
                string Descripcion; 
                String Dia;

                Dia = textDIA.Text;
                Hora = int.Parse(textBHORA.Text);
                Minuto = int.Parse(textBMINUTO.Text);
                Descripcion = textBDESCRIPCION.Text;

                try
                {
                    Cita obj = new Cita(Dia, Hora, Minuto, Descripcion);
                }
                catch (ArgumentException EX)
                {
                    MessageBox.Show(EX.Message);
                }

                MessageBox.Show("Su cita sera" + " el dia " + Dia + " " + " a las: " + Hora + ":" + Minuto + "    " +
                    "                                  DESCRIPCION :   " + Descripcion);
            }
            catch (FormatException ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
