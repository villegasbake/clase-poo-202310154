﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsException03
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnCalcula_Click(object sender, EventArgs e)
        {

            try
            {
                int num1;
                int num2;
                int resultado;
                num1 = int.Parse(txt1.Text);
                num2 = int.Parse(txt2.Text);
                resultado = num1 / num2;
                txt3.Text = resultado.ToString();
            }
            catch (DivideByZeroException ex)
            {
                MessageBox.Show("No puedes dividir por cero");
            }
            finally
            {
                MessageBox.Show("Ingresa otro numero o termina el programa");
            }
        }
    }
}
