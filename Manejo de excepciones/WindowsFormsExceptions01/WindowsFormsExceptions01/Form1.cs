﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsExceptions01
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnCalcula_Click(object sender, EventArgs e)
        {
            try
            {
                double numero1 = 0;
                double numero2 = 0;
                double result = 0;

                numero1 = Convert.ToDouble(txt1.Text);
                numero2 = Convert.ToDouble(txt2.Text);
                result = numero1 + numero2;
                lblResultado.Text = Convert.ToString(result);
            }
            catch (FormatException ex)
            {
                MessageBox.Show("Ingresa un valor correcto(numero)", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
