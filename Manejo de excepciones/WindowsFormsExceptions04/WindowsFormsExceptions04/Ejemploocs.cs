﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsExceptions04
{
    class Ejemploocs
    {
        public int a;

        public void CalculaDivision(int numerador, int denominador)
        {
            if (denominador == 0)
                throw new Exception("El denominador NO debe ser cero");
            else
                a = (numerador / denominador);
        }
    }
}
