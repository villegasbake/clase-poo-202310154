﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practica_Herencia
{
    class Herencia
    {
        //Abstracción de variables
        public int atributo1;
        private int atributo2;
        protected int atributo3;

        //Abstracción de un método publico 
        public void metodo1()
        {
            Console.WriteLine("Este es wl metodo de la clase herencia");
        }

        //Abstracción de un método con acceso private
        private void metodo2()
        {
            Console.WriteLine("Este metodo no es accesible fuera de la clase herencia");
        }
        //Abstracción de un método con acceso protected
        protected void metodo3()
        {
            Console.WriteLine("Este es el metodo 3 de la clase Herencia, tiene protected");
        }
        //Un método publico para usar el método con acceso private
        public void AccesoPrivate()
        {
            metodo2();
        }

    }
    //Abstracción de clase con propiedades de la clase Herencia
    class Hijo: Herencia
    {
        //Un método publico para usar el método con acceso protected
        public void accesoProtected()
        {
            metodo3();
        }
    }
}
