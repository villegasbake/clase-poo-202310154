﻿using System;

namespace Operaciones
{
    class Program
    {
        static void Main(string[] args)
        {
            int a, b;
            OperacionesAritmeticas oper = new OperacionesAritmeticas();
            Console.WriteLine("Teclea dos numeros");
            a = int.Parse(Console.ReadLine());
            b = int.Parse(Console.ReadLine());
            Console.WriteLine(oper.suma(a, b));
            Console.WriteLine(oper.resta(a, b));
            Console.WriteLine(oper.multiplica(a, b));
            Console.WriteLine(oper.divide(a, b));


        }
    }
}
