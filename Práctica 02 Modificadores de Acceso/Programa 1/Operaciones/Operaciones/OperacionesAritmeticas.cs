﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Operaciones
{
    class OperacionesAritmeticas
    {
        public double suma(double a, double b)
        {
            return a + b;
        }

        public double resta(double a, double b)
        {
            return a - b;
        }

        public double multiplica(double a, double b)
        {
            return a * b;
        }
        public double divide(double a, double b)
        {
            return a / b;
        }



    }
}
