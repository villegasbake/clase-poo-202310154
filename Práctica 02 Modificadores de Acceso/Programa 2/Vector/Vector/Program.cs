﻿using System;

namespace Vector
{
    class Program
    {
        static void Main(string[] args)
        {
            //Hacer un arreglo de 1x1 con 20 elementos con dos métodos: LeerDatos y MostrarDatos
            
            vector obj = new vector();
            obj.LeerDatos(); //Método en color verde
            obj.MostrarDatos(); //Método en color azul
            //Decidí usar colores para entender la ruta del código durante la ejecución en la consola

        }

        
    }
}
