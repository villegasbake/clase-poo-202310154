﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vector
{
    class vector
    {
        //Creación del arreglo
        //Lo creo en la raiz de la clase para ser aprovechado en todos los métodos
        int[] arreglo = new int[20];
        public void LeerDatos()
        {
            //Cambio los colores para diferenciar los métodos
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Lectura del arreglo");
            
            //Un for para leer los elementos o números del arreglo
            for (int i = 0; i < 20; i++)
            {
                Console.WriteLine("Ingresa un número");
                //Uso una variable auxiliar para almacenar datos en el arreglo 
                //No puedo almacenar directamente por los tipos de datos o alguna regla que desconozco
                int elemento = int.Parse(Console.ReadLine());
                arreglo[i] = elemento;
            }
        }

        public void MostrarDatos()
        {
            
            Console.ForegroundColor = ConsoleColor.Blue;
            //Uso un foreach para obtener cada valor del arreglo y poder imprimirlo
            Console.WriteLine("Impresión del arreglo:");
            foreach (int valores in arreglo)
            {
                Console.WriteLine(valores);
            }
            //Aquí vuelvo a cambiar el color de fuente para entender las partes del código
            Console.ForegroundColor = ConsoleColor.White;
        }
    }
}
