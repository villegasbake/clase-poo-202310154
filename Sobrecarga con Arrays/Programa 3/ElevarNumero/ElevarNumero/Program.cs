﻿using System;

namespace ElevarNumero
{
    class Program
    {
        static void Main(string[] args)
        {
            Elevar obj = new Elevar();
            obj.elevacion(5);
            obj.elevacion(10.50f);
            obj.elevacion(20.75d);
        }
    }
}
