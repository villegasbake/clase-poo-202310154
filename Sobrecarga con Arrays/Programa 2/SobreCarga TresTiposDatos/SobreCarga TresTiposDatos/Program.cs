﻿using System;

namespace SobreCarga_TresTiposDatos
{
    class Program
    {
        static void Main(string[] args)
        {
            //Programa con SobreCarga
            SobreCarga SB = new SobreCarga();
            SB.Grande(10,4);
            SB.Grande(15f, 35f, 23f);
            SB.Grande(10d, 12d, 12d, 18d);

        }
    }
}
