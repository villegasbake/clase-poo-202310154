﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SobreCarga_TresTiposDatos
{
    class SobreCarga
    {
       public SobreCarga()
        {
            Console.WriteLine("Números con tipos de variables");
        }
        public void Grande(int a, int b)
        {
            Console.WriteLine(a + "," + b);
        }

        public void Grande(float a, float b, float c)
        {
            Console.WriteLine(a + "," + b + "," + c);
        }

        public void Grande(double a, double b, double c, double d)
        {
            Console.WriteLine(a + "," + b + "," + c + "," + d);
        }

    }
}
