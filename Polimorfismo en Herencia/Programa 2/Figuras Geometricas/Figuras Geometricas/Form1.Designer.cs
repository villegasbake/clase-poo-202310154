﻿
namespace Figuras_Geometricas
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_CalcularArea = new System.Windows.Forms.Button();
            this.btn_Salir = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.radioRec = new System.Windows.Forms.RadioButton();
            this.radioCir = new System.Windows.Forms.RadioButton();
            this.radioTri = new System.Windows.Forms.RadioButton();
            this.txtLargo = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtAltura = new System.Windows.Forms.TextBox();
            this.txtRadio = new System.Windows.Forms.TextBox();
            this.txtAncho = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // btn_CalcularArea
            // 
            this.btn_CalcularArea.Location = new System.Drawing.Point(218, 32);
            this.btn_CalcularArea.Name = "btn_CalcularArea";
            this.btn_CalcularArea.Size = new System.Drawing.Size(75, 51);
            this.btn_CalcularArea.TabIndex = 0;
            this.btn_CalcularArea.Text = "Calcular Area";
            this.btn_CalcularArea.UseVisualStyleBackColor = true;
            this.btn_CalcularArea.Click += new System.EventHandler(this.btn_CalcularArea_Click);
            // 
            // btn_Salir
            // 
            this.btn_Salir.Location = new System.Drawing.Point(218, 133);
            this.btn_Salir.Name = "btn_Salir";
            this.btn_Salir.Size = new System.Drawing.Size(75, 51);
            this.btn_Salir.TabIndex = 1;
            this.btn_Salir.Text = "Salir";
            this.btn_Salir.UseVisualStyleBackColor = true;
            this.btn_Salir.Click += new System.EventHandler(this.btn_Salir_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(42, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(75, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Tipo de Figura";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(42, 162);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(93, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Datos de la Figura";
            // 
            // radioRec
            // 
            this.radioRec.AutoSize = true;
            this.radioRec.Location = new System.Drawing.Point(46, 71);
            this.radioRec.Name = "radioRec";
            this.radioRec.Size = new System.Drawing.Size(80, 17);
            this.radioRec.TabIndex = 4;
            this.radioRec.TabStop = true;
            this.radioRec.Text = "Rectángulo";
            this.radioRec.UseVisualStyleBackColor = true;
            this.radioRec.CheckedChanged += new System.EventHandler(this.radioButton1_CheckedChanged);
            // 
            // radioCir
            // 
            this.radioCir.AutoSize = true;
            this.radioCir.Location = new System.Drawing.Point(46, 94);
            this.radioCir.Name = "radioCir";
            this.radioCir.Size = new System.Drawing.Size(93, 17);
            this.radioCir.TabIndex = 5;
            this.radioCir.TabStop = true;
            this.radioCir.Text = "Circunferencia";
            this.radioCir.UseVisualStyleBackColor = true;
            this.radioCir.CheckedChanged += new System.EventHandler(this.radioCir_CheckedChanged);
            // 
            // radioTri
            // 
            this.radioTri.AutoSize = true;
            this.radioTri.Location = new System.Drawing.Point(46, 117);
            this.radioTri.Name = "radioTri";
            this.radioTri.Size = new System.Drawing.Size(71, 17);
            this.radioTri.TabIndex = 6;
            this.radioTri.TabStop = true;
            this.radioTri.Text = "Tríangulo";
            this.radioTri.UseVisualStyleBackColor = true;
            this.radioTri.CheckedChanged += new System.EventHandler(this.radioButton3_CheckedChanged);
            // 
            // txtLargo
            // 
            this.txtLargo.Location = new System.Drawing.Point(86, 193);
            this.txtLargo.Name = "txtLargo";
            this.txtLargo.Size = new System.Drawing.Size(53, 20);
            this.txtLargo.TabIndex = 7;
            this.txtLargo.TextChanged += new System.EventHandler(this.txtLargo_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(43, 193);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(34, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "Largo";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(43, 277);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(34, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "Altura";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(43, 248);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(35, 13);
            this.label5.TabIndex = 10;
            this.label5.Text = "Radio";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(43, 221);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(38, 13);
            this.label6.TabIndex = 11;
            this.label6.Text = "Ancho";
            // 
            // txtAltura
            // 
            this.txtAltura.Location = new System.Drawing.Point(86, 277);
            this.txtAltura.Name = "txtAltura";
            this.txtAltura.Size = new System.Drawing.Size(53, 20);
            this.txtAltura.TabIndex = 12;
            // 
            // txtRadio
            // 
            this.txtRadio.Location = new System.Drawing.Point(86, 248);
            this.txtRadio.Name = "txtRadio";
            this.txtRadio.Size = new System.Drawing.Size(53, 20);
            this.txtRadio.TabIndex = 13;
            // 
            // txtAncho
            // 
            this.txtAncho.Location = new System.Drawing.Point(86, 221);
            this.txtAncho.Name = "txtAncho";
            this.txtAncho.Size = new System.Drawing.Size(53, 20);
            this.txtAncho.TabIndex = 14;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(406, 309);
            this.Controls.Add(this.txtAncho);
            this.Controls.Add(this.txtRadio);
            this.Controls.Add(this.txtAltura);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtLargo);
            this.Controls.Add(this.radioTri);
            this.Controls.Add(this.radioCir);
            this.Controls.Add(this.radioRec);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btn_Salir);
            this.Controls.Add(this.btn_CalcularArea);
            this.Name = "Form1";
            this.Text = "Figuras Geometricas";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_CalcularArea;
        private System.Windows.Forms.Button btn_Salir;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.RadioButton radioRec;
        private System.Windows.Forms.RadioButton radioCir;
        private System.Windows.Forms.RadioButton radioTri;
        private System.Windows.Forms.TextBox txtLargo;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtAltura;
        private System.Windows.Forms.TextBox txtRadio;
        private System.Windows.Forms.TextBox txtAncho;
    }
}

