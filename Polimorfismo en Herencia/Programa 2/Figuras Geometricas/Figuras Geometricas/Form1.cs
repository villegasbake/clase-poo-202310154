﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Figuras_Geometricas
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btn_Salir_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            if (radioRec.Checked)
            {
                txtLargo.Enabled = true;
                txtAncho.Enabled = true;
                txtRadio.Enabled = false;
                txtAltura.Enabled = false;
            }
        }

        private void radioButton3_CheckedChanged(object sender, EventArgs e)
        {
            if (radioTri.Checked)
            {
                txtLargo.Enabled = true;
                txtAncho.Enabled = true;
                txtRadio.Enabled = false;
                txtAltura.Enabled = true;
            }
        }

        private void radioCir_CheckedChanged(object sender, EventArgs e)
        {
            if (radioCir.Checked)
            {
                txtLargo.Enabled = false;
                txtAncho.Enabled = false;
                txtRadio.Enabled = true;
                txtAltura.Enabled = false;
            }
        }

        private void txtLargo_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void btn_CalcularArea_Click(object sender, EventArgs e)
        {
            Rectangulo obj = new Rectangulo();
            obj.largo = double.Parse(txtLargo.Text);
            obj.ancho = double.Parse(txtAncho.Text);
            obj.CalcularArea();
            MessageBox.Show("El Area es = " + obj.area);
            Circunferencia obj2 = new Circunferencia();
            obj2.largo = double.Parse(txtRadio.Text);
            obj2.CalcularArea();
            MessageBox.Show("El Area es = " + obj2.area);
            Triangulo obj3 = new Triangulo();
            obj3.largo = double.Parse(txtLargo.Text);
            obj3.altura = double.Parse(txtAltura.Text);
            obj3.CalcularArea();
            MessageBox.Show("El Area es = " + obj3.area);
        }
    }
}
