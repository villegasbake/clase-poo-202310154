﻿using System;

namespace Ejemplo_Arreglos
{
    class Program
    {
        static void Main(string[] args)
        {
            int suma = 0;
            int[] catalogoNumeros = new int[10];

            for (int i=0;i<catalogoNumeros.Length;i++)
            {
                Console.WriteLine("Ingresa un número");
                int num = int.Parse(Console.ReadLine());
                catalogoNumeros[i] = num;
            }
            Console.WriteLine("Imprimiendo arreglo ...");
            foreach(int val in catalogoNumeros)
            {
                Console.WriteLine(val);
                suma += val;
            }
            Console.WriteLine("La suma del Arrego es " + suma);
        }
    }
}
