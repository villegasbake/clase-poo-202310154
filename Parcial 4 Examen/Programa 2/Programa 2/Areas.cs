﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Programa_2
{
    public abstract class Areas
    {
        public double lado;

        public virtual void ObtenerLado()
        {
            Console.WriteLine("Escribe un valor");
            lado = double.Parse(Console.ReadLine());
        }
        public virtual void CalcularArea()
        {
            lado = 0;
        }
    }
}
