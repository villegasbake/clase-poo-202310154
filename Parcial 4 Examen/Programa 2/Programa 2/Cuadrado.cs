﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Programa_2
{
    class Cuadrado:Areas
    {
        public override void ObtenerLado()
        {
            Console.WriteLine("Escribe un lado");
            lado = double.Parse(Console.ReadLine());
        }
        public override void CalcularArea()
        {
            double area = lado * lado;
            Console.WriteLine("El area es " + area);
        }
    }
}
