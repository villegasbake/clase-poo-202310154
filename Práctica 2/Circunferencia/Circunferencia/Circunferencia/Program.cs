﻿using System;

namespace Circunferencia
{
    class Program
    {
        
        static void Main(string[] args)
        {
           
            // 2) Declare dos Objetos Rueda y Moneda
            Circunferencia Rueda = new Circunferencia();
            Circunferencia Moneda = new Circunferencia();

            // a)
            Rueda.setRadio(10);
            double radioRueda = Rueda.getRadio();

            // b) 
            Moneda.setRadio(1);
            double radioMoneda = Moneda.getRadio();

            // c)
            Rueda.Calcular_Area();

            // d) 
            Moneda.Calcular_Area();

            // e)
            Rueda.Calcular_Perimetro();

            // F)
            Moneda.Calcular_Perimetro();
            

            Console.WriteLine("Gracias por usarme, bye");
            Console.ReadKey();
        }

        

    }
}
