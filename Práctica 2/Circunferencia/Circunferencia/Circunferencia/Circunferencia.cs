﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Circunferencia
{

    class Circunferencia
    {

        public double radio = 0;

       
        public void setRadio(double radio)
        {
            this.radio = radio;
        }

        public double getRadio()
        {
            return this.radio;
        }

        


        public void Calcular_Area()
        {
            
           double Area = 0;
            Area = (radio*radio)*(Math.PI);
            Console.WriteLine("El valor de la area es de " + Area);
        }

        public void Calcular_Perimetro()
        {
            double Perimetro = 0;
            Perimetro = (radio + radio) * (Math.PI);
            Console.WriteLine("El valor del perimetro es de " + Perimetro); 
        }


    }
}
