﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Examen
{
    class Vehiculo
    {
        public int llantas = 0;
        int tanqueGasolina = 1;
        int llaveEncendido = 1;

        public void encenderVehiculo()
        {
            Console.WriteLine("Arrancando Vehiculo");
            Console.WriteLine("Vehiculo encendiendo");
        }

        protected void encenderLuz()
        {
            Console.WriteLine("Luz encendida");
        }

        protected void gastarGasolina()
        {
            Console.WriteLine("Gastando gasolina");
        }

        private void encenderRadio()
        {
            Console.WriteLine("Encendiendo Radio");
            //No todos los vehiculos tienen Radio, razón para quedarse private
        }

        ~Vehiculo()
        {
            Console.WriteLine("Vehículo enfriando");
            System.Console.WriteLine("Vehículo enfriando");
        }
    }
}
