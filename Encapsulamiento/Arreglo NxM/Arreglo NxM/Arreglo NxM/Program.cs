﻿using System;

namespace Arreglo_NxM
{
    class Program
    {
        static void Main(string[] args)
        {
            
            int n, m;
            //Lectura de valores para la matriz
            Console.WriteLine("Ingresa el valor n del arreglo");
            n = int.Parse(Console.ReadLine());
            Console.WriteLine("Ingresa el valor m del arrelgo");
            m = int.Parse(Console.ReadLine());

            //Auxiliares para manipular la matriz
            //con variables en vez de números predeterminado en el for
            int aux1 = n; int aux2 = m;

            //Creación de la Matriz
            int[,] NxM = new int[n,m];

            //Leer valores de la Matriz
            for (int a=0; a< aux1; a++)
            {
                for (int b=0; b<aux2; b++)
                {
                    Console.WriteLine("Ingresa un número");
                    int num = int.Parse(Console.ReadLine());
                    NxM[a, b] = num;
                }
            }

            //For para imprimir
            Console.WriteLine("Valores de la matriz:");
            for (int a = 0; a < aux1; a++)
            {
                for (int b = 0; b < aux2; b++)
                {
                    
                    Console.WriteLine(NxM[a, b]);
                }
            }


        }
    }
}
