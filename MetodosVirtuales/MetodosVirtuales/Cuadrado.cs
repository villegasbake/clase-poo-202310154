﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetodosVirtuales
{
    class Cuadrado : Triangulo
    {
        public override void CalculaArea(double b, double a)
        {
            double resultado = b * a;
            Console.WriteLine("El area del cuadrado es: {0}", resultado);
        }
    }
}
