﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetodosVirtuales
{
    class Triangulo
    {
        public virtual void CalculaArea(double b, double a)
        {
            double resultado = (b * a) / 2;
            Console.WriteLine("El area del triangulo es {0}", resultado);
        } 
    }
}
