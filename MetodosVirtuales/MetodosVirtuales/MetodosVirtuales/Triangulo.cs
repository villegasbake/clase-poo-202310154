﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetodosVirtuales
{
    class Triangulo
    {
        public virtual void Area(double b, double a)
        {
            double resultado = (b * a) / 2;
            Console.WriteLine("El resultado del Area del triangulo es " + resultado);
        }
    }
}
